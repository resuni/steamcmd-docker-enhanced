FROM steamcmd/steamcmd:ubuntu-20

# Install Source SDK Base 2013 Dedicated Server
RUN steamcmd +force_install_dir /app +login anonymous +app_update 244310 +quit

# Create symlinks to missing shared objects
# https://iraizo.github.io/tf2-classic-linux-server-tutorial/#important-create-symlinks-to-missing-shared-objects
WORKDIR "/app/bin"
RUN \
  ln -s datacache_srv.so datacache.so && \
  ln -s dedicated_srv.so dedicated.so && \
  ln -s engine_srv.so engine.so && \
  ln -s materialsystem_srv.so materialsystem.so && \
  ln -s replay_srv.so replay.so && \
  ln -s scenefilecache_srv.so scenefilecache.so && \
  ln -s shaderapiempty_srv.so shaderapiempty.so && \
  ln -s studiorender_srv.so studiorender.so && \
  ln -s vphysics_srv.so vphysics.so && \
  ln -s soundemittersystem_srv.so soundemittersystem.so

# Download TF2 classic
# No one mirror is reliable enough to automate download in Dockerfile. Find a
# list of offial mirrors in Discord and download manually. At the time of writing, this mirror worked:
# https://gg.apple-shack.org/tf2c/tf2classic-latest.zip
WORKDIR "/app"
COPY tf2classic-latest.zip .

# RUN things:
# * Install package dependencies
# * Symlink steamclient.so to work around "No such file or directory" issue
# * Extract TF2 classic and delete archive
# * Remove p7zip package only needed for build
RUN \
  apt-get update && \
  apt-get install -y libncurses5:i386 p7zip-full socat && \
  mkdir -p /root/.steam/sdk32 && \
  ln -s /root/.steam/steamcmd/linux32/steamclient.so /root/.steam/sdk32/steamclient.so && \
  7z x tf2classic-latest.zip && \
  rm -f tf2classic-latest.zip && \
  apt-get remove -y p7zip-full p7zip

# Launch
# The "-t3600 is to override socat's default timeout of 0.5 seconds - an ugly hack.
ENTRYPOINT ["socat", "-t3600", "STDIO", "EXEC:'./srcds_run -game tf2classic +maxplayers 24 +map pl_badwater',pty,ctty,stderr,echo=0"]
