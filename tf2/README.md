# Team Fortress 2 Dedicated Server

Dockerfile builds a functioning TF2 server.

## Build & run

```bash
docker build -t tf2 .
docker run -i -p 27015:27015/tcp -p 27015:27015/udp tf2
```

At the time of writing this, the final image size is roughly 10 GB.

## Notes

* Install libcurl3-gnutls:i386 to resolve "Could not load: replay_srv.so" error - https://forums.alliedmods.net/showthread.php?t=294141&page=2
