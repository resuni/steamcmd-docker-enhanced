# steamcmd-dockerfiles

Dockerfile examples for various game servers installed via steamcmd.

I've found that getting Steam game servers to run in Docker can be incredibly frustrating. I decided to sink a ton of time figuring out some tricks into making them work. Hopefully these notes and examples can help others get their game servers working. 

[Steam Application ID reference](https://developer.valvesoftware.com/wiki/Steam_Application_IDs)

## Things I've learned

* Don't bother using anything other than the Ubuntu images. While Ubuntu wouldn't normally be my first choice for a Docker container (or anything, really), the Ubuntu steamcmd images seem to require the least amount of effort to get things working.
* Older versions of Metamod and Sourcemod need the ubuntu-18 image. With enough time you might be able to figure out what dependencies to install to make them work on ubuntu-20, but ubuntu-18 is the quick solution.
* A LOT of disk space is required to build these images - more than double the final build size, especially if you plan on building more than one. When I build these, I spin up a 160 GB VM on [Vultr](https://www.vultr.com/?ref=6869073) or [Linode](https://www.linode.com/?r=3540c3787a316f3ac6b21d573c74af373aecb4c2).
* You almost always need to symlink steamclient.so to wherever srcds_run is looking for it. Examples of this can be found in every Dockerfile. This does not eliminate the error complaining about not being able to contact Steam, which happens even when things are working.
* If Metamod does not load, the error mentioning ELFCLASS64 has nothing to do with whatever the actual problem is. This error appears even when it does load.
* Not specific to steamcmd, but on Arch Linux I had an issue where containers were being limited to 10 GB. This was caused by the default storage driver being `devicemapper`, and was easily fixed by [changing it to `overlay2`](https://docs.docker.com/storage/storagedriver/overlayfs-driver/).

## Working around srcds_run's bypassing of STDIO

Rather than outputting to stdout, srcds_run writes directly to the Terminal. [This GitHub issue](https://github.com/ValveSoftware/Source-1-Games/issues/1684) is the best explanation I can find about the issue. The users in that issue are running Windows, but the problem seems to be the same on Linux.

This means when running in Docker, the output will delayed, malformed, and you won't be able to type commands, or even use Ctrl+C to kill the container. I use [socat](http://www.dest-unreach.org/socat/doc/socat.html) to work around this annoying limitation. Socat is a tool that establishes two bidirectional byte streams and transfers data between them. In this case, srcds_run wants to write directly to a TTY, so we use socat to provide it with a PTY and direct that to STDIO. Example: 
```Dockerfile
ENTRYPOINT ["socat", "-t3600", "STDIO", "EXEC:'./srcds_run <whatever options>',pty,ctty,stderr,echo=0"]
```

Breakdown of this command:
* `-t3600` - socat has a default timeout of 0.5 seconds, which means it exits if it does not detect activity after 0.5 seconds. Before this was changed to 3600 seconds (1 hour), the container would quit almost immediately due to lack of output half a second after the process started. A better solution would be to completely disable this timeout, but I couldn't figure out a way to do that.
* `STDIO` - One end of the byte stream is STDIO (STDIN/STDOUT). Docker interfaces with this end of the byte stream.
* `EXEC` - The other end of the byte stream. Contains the command to run to execute the game server, as well as some other necessary flags explained below.
  * `pty` - Creates a PTY. srcds_run wants to send to a terminal. Specify this to give it one.
  * `ctty` - Makes the PTY the controlling TTY of the sub process. This needs to be specified in order for the game server process to accept commands through socat's STDIN.
  * `stderr` - Redirects stderr to stdout. Without this set, errors sent to stderr would not appear.
  * `echo=0` - This is supposed to disable echoing back of sent commands, but it doesn't seem to work.

In addition to socat, the Docker container needs to be started in interactive mode to receive commands. Simply pass `-i` to `docker run` when starting the container. Even without this flag, the output will still look correct. Only the input will be disabled.

### Alternative workaround using GNU screen

I won't document this extensively here, since socat is a better solution, but I want to mention the [GNU screen](https://www.gnu.org/software/screen/) workaround in case socat doesn't work out. Screen can be configured to log whatever is sent into it. Using a Dockerfile `RUN` statement, put a line in .screenrc to set the logfile to whatever destination you want. Then, make a separate script with contents similar to the following, and set your `ENTRYPOINT` to this script: `screen -dmL ./srcds_run <whatever options> && tail -F /root/console.log`

There are problems with this solution:
* The classic problems that come with multiple processes running inside a container. The container is going to stop when the tail process does, which is never. If the game server quits, the container will continue to run tail, watching a console.log that will never again be updated.
* Commands cannot be sent through STDIN. In order to control the server, you'll have to use RCON, or gain access to the server's console by executing `screen -dr` inside the container.

Try to use socat if you can, but screen may be an adequate workaround if that doesn't work.

## Improvements

I've mentioned most of these things already, but here's a more concise/complete list:

* Find a way to make the server not run as root. I spent some time on this, and no matter what I did, steamcmd always wanted to write things to root's home directory.
* Find a way to completely disable socat timeouts, rather than just making timeouts very long. I can see this being a problem for servers that run for a long period of time without seeing a lot of player activity (e.g., overnight). Passing the `keepalive` option didn't work, since it only applies to socket communications and not PTY communications.
* Disable echoing back of commands. I tried with `echo=0`, it didn't work, and the minor annoyance isn't worth the effort right now.
